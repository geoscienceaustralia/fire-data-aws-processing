import boto3
import os
import numpy as np

TILE_FILE = "act.txt"
TILES = []

SQS_QUEUE = os.environ.get("SQS_QUEUE", 'dea-sandbox-eks-fmc')

sqs = boto3.resource('sqs')
queue = sqs.get_queue_by_name(QueueName=SQS_QUEUE)


def load_tiles(tile_file):
    with open(tile_file, 'r') as f:
        return [l.strip() for l in f]


def add_items():
   
    for tile in TILES:
        print(f"adding {tile}")
        queue.send_message(MessageBody=tile)


if __name__ == "__main__":
    print("Starting to add to queue")

    TILES = load_tiles(TILE_FILE)

    add_items()
