FROM geoscienceaustralia/sandbox:latest
RUN pip3 install --upgrade pip
RUN pip3 install geopandas sklearn dask==2.10.1
RUN pip3 install --extra-index-url="https://packages.dea.ga.gov.au" odc-algo
RUN pip3 install onnxruntime

WORKDIR /home/jovyan
COPY Scripts/ Scripts/
COPY helper/ helper/
COPY /process_fmc.py /home/jovyan/process_fmc.py
COPY /randomforest.onnx /home/jovyan/randomforest.onnx

ENV PYTHONPATH='/home/jovyan/Scripts':'/home/jovyan/helper/':'/home/jovyan/'

CMD [ "python3", "process_fmc.py" ]
