# Fuel Moisture Content AWS

## Purpose

Run ANU-WALD Fuel Moisture Content workflow on DEA AWS data


1. Generate docker image using bitbucket pipelines.

2. Ensure that [Terraform YAML](https://bitbucket.org/geoscienceaustralia/datakube/src/master/workspaces/dea-sandbox/03_odc_k8s_apps/fmc_sentinel.tf) in Datakube has correct permissions for S3 objects.

3. Modify [deployment YAML](https://bitbucket.org/geoscienceaustralia/datakube-apps/src/master/workspaces/dea-sandbox/processing/08_fmc.yaml) in Datakube-apps to contain suitable S3 path, docker image name (collect from docker hub) and pod resources.

4. Add items to queue using `python3 buildqueue.py` and parse in desired study site using text file.

5. Sync output items to EC2 instance `aws s3 sync s3://dea-public-data-dev/fmc fmc` and download using folder explorer.