# Process LCCS based on tiles on SQS

import os
import sys
import boto3
import yaml
import logging
import numpy as np
import pandas
import time
import datacube
from datacube.utils import cog

from assume_role_helper import get_autorefresh_session
import sys
sys.path.append('../Scripts')
from dea_datahandling import load_ard, array_to_geotiff
import onnxruntime
# PRODUCT = 's2b_nrt_granule'
PRODUCT = 's2a_ard_granule'
BANDS=["nbar_blue","nbar_green","nbar_red", "nbart_red_edge_1","nbart_red_edge_2","nbart_red_edge_3", "nbar_nir_1","nbar_nir_2", "nbar_swir_2","nbar_swir_3"]
S3_PATH=os.environ.get("S3_PATH", "fmc")
SQS_QUEUE = os.environ.get("SQS_QUEUE", 'dea-sandbox-eks-fmc')
S3_BUCKET = os.environ.get("S3_BUCKET", 'dea-public-data-dev')
model = onnxruntime.InferenceSession("randomforest.onnx")

VISIBILITYTIMEOUT = os.environ.get('VISIBILITYTIMEOUT', 3600)

# Set up the queue
if 'AWS_ROLE_ARN' in os.environ and 'AWS_WEB_IDENTITY_TOKEN_FILE' in os.environ:
    role_with_web_identity_params = {
        "DurationSeconds": os.getenv('SESSION_DURATION', 3600),
        "RoleArn": os.getenv('AWS_ROLE_ARN'),
        "RoleSessionName": os.getenv('AWS_SESSION_NAME', 'test_session'),
        "WebIdentityToken": open(os.getenv('AWS_WEB_IDENTITY_TOKEN_FILE')).read(),
    }
    autorefresh_session = get_autorefresh_session(**role_with_web_identity_params)
    sqs = autorefresh_session.resource('sqs')
else:
    sqs = boto3.resource('sqs')

queue = sqs.get_queue_by_name(QueueName=SQS_QUEUE)

def get_tile_bounds(lat1,lat2,lon1,lon2,year):
    return {'lat':(lat1, lat2),  'lon':(lon1,lon2), 'time':year}

def upload_to_s3(path, files):
    logging.info("Commencing S3 upload")

    # Set up the s3 resource
    if 'AWS_ROLE_ARN' in os.environ and 'AWS_WEB_IDENTITY_TOKEN_FILE' in os.environ:
        role_with_web_identity_params = {
            "DurationSeconds": os.getenv('SESSION_DURATION', 3600),
            "RoleArn": os.getenv('AWS_ROLE_ARN'),
            "RoleSessionName": os.getenv('AWS_SESSION_NAME', 'test_session'),
            "WebIdentityToken": open(os.getenv('AWS_WEB_IDENTITY_TOKEN_FILE')).read(),
        }
        autorefresh_session = get_autorefresh_session(**role_with_web_identity_params)
        s3r = autorefresh_session.resource('s3')
    else:
        s3r = boto3.resource('s3')

    if S3_BUCKET:
        logging.info("Uploading to {}".format(S3_BUCKET))
        # Upload data
        for out_file in files:
            data = open(out_file, 'rb')
            key = "{}/{}".format(path, os.path.basename(out_file))
            logging.info("Uploading file {} to S3://{}/{}".format(out_file, S3_BUCKET, key))
            s3r.Bucket(S3_BUCKET).put_object(Key=key, Body=data, ACL="bucket-owner-full-control")
            os.remove(out_file)
    else:
        logging.warning("Not uploading to S3, because the bucket isn't set.")

def count_messages():
    queue.load()
    return int(queue.attributes["ApproximateNumberOfMessages"])

if __name__ == "__main__":
    n_messages = count_messages()

    #conf_template = load_config(configpath)
    logging.info("start reading messages")
    while n_messages > 0:
        messages = queue.receive_messages(
            VisibilityTimeout = VISIBILITYTIMEOUT,
            MaxNumberOfMessages = 1)

        if len(messages) > 0:
            message = messages[0]
            logging.info("Message Available")
            # break message into tile / year
            lat1,lat2,lon1,lon2, year = message.body.split()
            logging.info(f"Processing for {year}")
            query = get_tile_bounds(lat1,lat2,lon1,lon2, year)

            out_filename = "fmc"+lat1.replace('-', '_')+ '_' + lon1 + '_' + year.replace('-', '_') + '.tif'

            dc=datacube.Datacube(app='Using_load_ard')
            ds = load_ard(dc=dc,
                       products=[PRODUCT],
                       measurements=BANDS,          
                       group_by='solar_day',        # Fuse all datasets captured on the same day into one raster plane
                       output_crs='EPSG:3577',
                       #crs='EPSG:3577',
                       resolution=(-10, 10),
                       dask_chunks={},
                       **query
                      )
            
            ds['ndvi']=((ds.nbar_nir_1-ds.nbar_red)/(ds.nbar_nir_1+ds.nbar_red))
            ds['ndii']=((ds.nbar_nir_1-ds.nbar_swir_2)/(ds.nbar_nir_1+ds.nbar_swir_2))
   
            refl = ds[['ndvi','ndii','nbar_red','nbar_green','nbar_blue','nbar_nir_1','nbar_nir_2','nbar_swir_2','nbar_swir_3']].to_array().values
            refl_rf = refl.reshape((9,-1)).swapaxes(0,1)
            
            # Note, input data is converted from float64 to float32. However, most values are integers.
            rf_fmc = model.run(['fmc'], {'float_input': refl_rf.astype('f4')})[0]
            # Reshape and remove dimensions of 1 if present (time)
            fmc = rf_fmc.reshape(refl.shape[1:]).squeeze()
           
            transform=ds.geobox.transform.to_gdal()
            projection=ds.geobox.crs.wkt

            array_to_geotiff(out_filename, fmc, geo_transform=transform, projection=projection, nodata_val=np.nan )
            
            files = [out_filename]

            # upload to S3
            upload_to_s3(S3_PATH, files)

            message.delete()
        else:
            logging.info(" No messages found")
        
        n_messages = count_messages()
