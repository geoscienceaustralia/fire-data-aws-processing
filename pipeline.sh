#!/bin/bash
export TAG=$(git describe --always)
export IMAGE_NAME=geoscienceaustralia/fuel-moisture-content:$TAG
docker build . -t $IMAGE_NAME

while getopts ":p" opt; do
  case ${opt} in
    p )
      echo "Pushing to Docker hub" >&2
      docker login --username $DOCKER_HUB_USERNAME --password $DOCKER_HUB_PASSWORD
      # push the new Docker image to the Docker registry
      docker push $IMAGE_NAME
#      docker push $IMAGE_LATEST
      ;;
  esac
done

